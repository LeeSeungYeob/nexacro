package hello.hellospring.controller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@Controller
public class CrawlingController {
    @RequestMapping(value = "/jsoupcrawler")
    public String crawling(Model model, @RequestParam String keyword){
        System.out.println("aaaaa");
        // 크롤링 작업 수행
        String result = "크롤링 결과"; // 크롤링 결과를 나타내는 문자열
        model.addAttribute("result", result); // 결과 값을 모델에 추가
        return "result"; // 결과를 보여줄 뷰 이름 반환
    }
}
