package hello.hellospring.controller;


import com.nexacro17.xapi.data.DataSet;
import com.nexacro17.xapi.data.PlatformData;
import com.nexacro17.xapi.tx.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.nexacro17.xapi.data.DataTypes;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import DB.DBConnection;


@Controller
public class NexaController {
    /*
            데이터셋의 정보를 가져오는 함수
     */
    private List<Map<String, Object>> processDataset(PlatformData in_pData, String datasetName) {
        List<Map<String, Object>> resultList = new ArrayList<>();
        DataSet dataset = in_pData.getDataSet(datasetName);
        if (dataset != null) {
            int rowCount = dataset.getRowCount();
            int columnCount = dataset.getColumnCount();

            for (int row = 0; row < rowCount; row++) {
                Map<String, Object> rowMap = new HashMap<>();
                for (int col = 0; col < columnCount; col++) {
                    String columnName = dataset.getColumn(col).getName();
                    Object value = dataset.getObject(row, col);
                    rowMap.put(columnName, value);
                }
                resultList.add(rowMap);
            }
        }
        return resultList;
    }

    /*
        데이터셋을 생성하는 함수
     */
    private DataSet createDataSet(String datasetName, Map<String, Integer> columns) {
        DataSet dataset = new DataSet(datasetName);

        // 열(column) 추가
        for (Map.Entry<String, Integer> entry : columns.entrySet()) {
            String columnName = entry.getKey();
            Integer columnType = entry.getValue();
            dataset.addColumn(columnName, columnType);
        }

        return dataset;
    }

    /*
        데이터셋의 로우를 생성하는 함수
     */
    private void addRowToDataSet(DataSet dataset, Map<String, Object> rowData) {
        int row = dataset.newRow();

        // 행(row)의 데이터 설정
        for (Map.Entry<String, Object> entry : rowData.entrySet()) {
            String columnName = entry.getKey();
            Object value = entry.getValue();
            dataset.set(row, columnName, value);
        }
    }

    /*
        데이터셋 nexacro로 respones
     */
    private void sendDataSetResponse(HttpServletResponse response, DataSet dataset) throws PlatformException {
        HttpPartPlatformResponse res = new HttpPartPlatformResponse(response);
        res.sendDataSet(dataset);
        res.end();
    }


    @GetMapping("/")
    public String hello(Model model)
    {
        return "redirect:/index.html";
    }

    @RequestMapping(value = "/test")
    @CrossOrigin(origins = {"http://localhost:8080", "http://127.0.0.1:8080"})
    public void request(HttpServletRequest request, HttpServletResponse response) throws PlatformException {

        // ************************ 1.nexa에서 request **********************************//
        try {
            HttpPlatformRequest pReq = new HttpPlatformRequest(request);
            pReq.receiveData(); // 데이터 받는다
            PlatformData in_pData = pReq.getData(); // 데이터 가져온다

            // 데이터셋 내용 확인 및 리스트로 반환
            List<Map<String, Object>> resultList = processDataset(in_pData, "Dataset00");

            System.out.println(resultList);

            // 리스트를 활용하여 원하는 작업 수행
            for (Map<String, Object> row : resultList) {
                System.out.println("idx:" + row.get("idx") + " name:" + row.get("name"));
            }

        // ************************ 2.Spring에서 respnse **********************************//
            Map<String, Integer> columns = new HashMap<>();
            columns.put("idx", DataTypes.INT);
            columns.put("name", DataTypes.STRING);

            DataSet Dataset01 = createDataSet("Dataset01", columns);
            Object[][] rowData = {
                    {0, "일승엽"},
                    {1, "이승엽"},
                    {2, "삼승엽"}
            };

            for (Object[] row : rowData) {
                Map<String, Object> data_map = new HashMap<>();
                data_map.put("idx", row[0]);
                data_map.put("name", row[1]);
                addRowToDataSet(Dataset01, data_map);
            }

            sendDataSetResponse(response, Dataset01);


        // ************************ 3.DB에 있는 정보 nexa로 올리기 **********************************//
            String NOTICE_TABLE = "company_info";
            String selectNoticeQuery = "SELECT * FROM " + NOTICE_TABLE;
            List<Object[]> result = (List<Object[]>) DBConnection.execute_query(selectNoticeQuery, null, "fetchall");

            //nexacro로 전송할 데이터셋 컬럼 생성
            columns = new HashMap<>();
            columns.put("Company_Id", DataTypes.INT);
            columns.put("Company_Pw", DataTypes.STRING);
            columns.put("Company_Name", DataTypes.STRING);
            columns.put("Ceo_Name", DataTypes.STRING);
            columns.put("Company_RegNo", DataTypes.STRING);
            columns.put("Company_Tel", DataTypes.STRING);
            columns.put("Concern_Name", DataTypes.STRING);
            columns.put("Concern_Tel", DataTypes.STRING);
            columns.put("Company_Addr", DataTypes.STRING);
            DataSet Dataset02 = createDataSet("Dataset02", columns);

            //데이터셋에 넣을 object 객체 생성
            rowData = new Object[result.size()][];

            for (int i = 0; i < result.size(); i++) {
                Object[] row = result.get(i);
                rowData[i] = new Object[row.length];

                for (int j = 0; j < row.length; j++) {
                    rowData[i][j] = row[j];
                }
            }

            //Object 객체에 데이터 삽입 및 데이터셋에 삽입
            for (Object[] row : rowData) {
                Map<String, Object> dataMap = new HashMap<>();
                dataMap.put("Company_Id", row[0]);
                dataMap.put("Company_Pw", row[1]);
                dataMap.put("Company_Name", row[2]);
                dataMap.put("Ceo_Name", row[3]);
                dataMap.put("Company_RegNo", row[4]);
                dataMap.put("Company_Tel", row[5]);
                dataMap.put("Concern_Name", row[6]);
                dataMap.put("Concern_Tel", row[7]);
                dataMap.put("Company_Addr", row[8]);
                addRowToDataSet(Dataset02, dataMap);
            }
            System.out.println(Dataset02.getObject(0,4));

           // 데이터셋 전송
            sendDataSetResponse(response, Dataset02);

        } catch (Exception e) {
            // TODO: handle exception
            e.getStackTrace();
            System.out.println(e);
        }
    }
}