package DB;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBConnection {
    static String url = "jdbc:mysql://211.251.237.148:3306/ipharmdb";
    static String username = "root";
    static String password = "admin";
    static Connection conn = null;

    public static Object execute_query(String query, Object[] params, String method) throws SQLException {
        conn = DriverManager.getConnection(url, username, password);
        long start_time = System.currentTimeMillis();
        try {
            PreparedStatement pstmt = conn.prepareStatement(query);
            if (params != null) {
                for (int i = 0; i < params.length; i++) {
                    pstmt.setObject(i + 1, params[i]);
                }
            }
            if (method.equals("execute")) {
                pstmt.execute();
                return null;
            } else {
                ResultSet rs = pstmt.executeQuery();
                List<Object[]> result = new ArrayList<>();
                while (rs.next()) {
                    int columnCount = rs.getMetaData().getColumnCount();
                    Object[] row = new Object[columnCount];
                    for (int i = 0; i < columnCount; i++) {
                        row[i] = rs.getObject(i + 1);
                    }
                    result.add(row);
                }
                if (method.equals("fetchall")) {
                    return result;
                } else if (method.equals("fetchone")) {
                    return result.size() > 0 ? result.get(0) : null;
                } else if (method.equals("fetchmany")) {
                    return result.size() > 0 ? result.subList(0, pstmt.getMaxRows()) : null;
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            long end_time = System.currentTimeMillis();
            System.out.println("Execution time: " + (end_time - start_time) + "ms");
        }
    }
}