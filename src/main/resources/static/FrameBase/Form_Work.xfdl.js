(function()
{
    return function()
    {
        if (!this._is_form)
            return;
        
        var obj = null;
        
        this.on_create = function()
        {
            this.set_name("Form_Work");
            this.set_titletext("Form_Work");
            if (Form == this.constructor)
            {
                this._setFormPosition(1280,720);
            }
            
            // Object(Dataset, ExcelExportObject) Initialize
            obj = new Dataset("Dataset00", this);
            obj._setContents("<ColumnInfo><Column id=\"idx\" type=\"STRING\" size=\"256\"/><Column id=\"name\" type=\"STRING\" size=\"256\"/></ColumnInfo><Rows><Row><Col id=\"idx\">1</Col><Col id=\"name\">대리님</Col></Row><Row><Col id=\"idx\">2</Col><Col id=\"name\">주임님</Col></Row><Row><Col id=\"idx\">3</Col><Col id=\"name\">사원님</Col></Row><Row><Col id=\"idx\">4</Col><Col id=\"name\">사원님</Col></Row><Row><Col id=\"idx\">5</Col><Col id=\"name\">5</Col></Row><Row><Col id=\"idx\">6</Col><Col id=\"name\">5</Col></Row><Row><Col id=\"idx\">7</Col><Col id=\"name\">5</Col></Row><Row><Col id=\"idx\">8</Col><Col id=\"name\">5</Col></Row></Rows>");
            this.addChild(obj.name, obj);


            obj = new Dataset("Dataset01", this);
            obj._setContents("<ColumnInfo><Column id=\"idx\" type=\"INT\" size=\"256\"/><Column id=\"name\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);


            obj = new Dataset("Dataset02", this);
            obj._setContents("<ColumnInfo><Column id=\"Company_Id\" type=\"STRING\" size=\"256\"/><Column id=\"Company_Pw\" type=\"STRING\" size=\"256\"/><Column id=\"Company_Name\" type=\"STRING\" size=\"256\"/><Column id=\"Ceo_Name\" type=\"STRING\" size=\"256\"/><Column id=\"Company_RegNo\" type=\"STRING\" size=\"256\"/><Column id=\"Company_Tel\" type=\"STRING\" size=\"256\"/><Column id=\"Concern_Name\" type=\"STRING\" size=\"256\"/><Column id=\"Concern_Tel\" type=\"STRING\" size=\"256\"/><Column id=\"Company_Addr\" type=\"STRING\" size=\"256\"/></ColumnInfo>");
            this.addChild(obj.name, obj);
            
            // UI Components Initialize
            obj = new Button("Button00","414","10","212","64",null,null,null,null,null,null,this);
            obj.set_taborder("0");
            obj.set_text("Button00");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid00","4","4","400","286",null,null,null,null,null,null,this);
            obj.set_taborder("1");
            obj.set_binddataset("Dataset01");
            obj.set_autofittype("col");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"80\"/><Column size=\"80\"/></Columns><Rows><Row band=\"head\" size=\"24\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell text=\"idx\"/><Cell col=\"1\" text=\"name\"/></Band><Band id=\"body\"><Cell text=\"bind:idx\"/><Cell col=\"1\" text=\"bind:name\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);

            obj = new Grid("Grid01","5","301","627","331",null,null,null,null,null,null,this);
            obj.set_taborder("2");
            obj.set_binddataset("Dataset02");
            obj.set_autofittype("col");
            obj.set_autosizingtype("row");
            obj._setContents("<Formats><Format id=\"default\"><Columns><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/><Column size=\"80\"/></Columns><Rows><Row size=\"24\" band=\"head\"/><Row size=\"24\"/></Rows><Band id=\"head\"><Cell text=\"회사아이디\"/><Cell col=\"1\" text=\"회사비밀번호\"/><Cell col=\"2\" text=\"회사이름\"/><Cell col=\"3\" text=\"CEO이름\"/><Cell col=\"4\" text=\"사업자번호\"/><Cell col=\"5\" text=\"회사 전화번호\"/><Cell col=\"6\" text=\"담당자\"/><Cell col=\"7\" text=\"담당자번호\"/><Cell col=\"8\" text=\"회사주소\"/></Band><Band id=\"body\"><Cell text=\"bind:Company_Id\"/><Cell col=\"1\" text=\"bind:Company_Pw\"/><Cell col=\"2\" text=\"bind:Company_Name\"/><Cell col=\"3\" text=\"bind:Ceo_Name\"/><Cell col=\"4\" text=\"bind:Company_RegNo\"/><Cell col=\"5\" text=\"bind:Company_Tel\"/><Cell col=\"6\" text=\"bind:Concern_Name\"/><Cell col=\"7\" text=\"bind:Concern_Tel\"/><Cell col=\"8\" text=\"bind:Company_Addr\"/></Band></Format></Formats>");
            this.addChild(obj.name, obj);
            // Layout Functions
            //-- Default Layout : this
            obj = new Layout("default","Desktop_screen",1280,720,this,function(p){});
            this.addLayout(obj.name, obj);
            
            // BindItem Information

            
            // TriggerItem Information

        };
        
        this.loadPreloadList = function()
        {

        };
        
        // User Script
        this.registerScript("Form_Work.xfdl", function() {

        this.Button00_onclick = function(obj,e)
        {
        	/*alert("테스트");*/
        	this.callMethod();
        };

        this.callMethod = function()
        {
        	var rowCount = this.Dataset00.getRowCount();


        	for (var i = 0; i < rowCount; i++) {
        		trace(this.Dataset00.getColumn(i,1))
        		trace('안녕요ㅎㅎㅎㅎㅎㅎ')
        	}
          var sId    = "baseId";
          var sUrl   = "http://localhost:8080/test";
          var sInDs  = "Dataset00=Dataset00";
          var sOutDs = "Dataset01=Dataset01 Dataset02=Dataset02";
          var sArg   = "";
          var sfunc  = "fn_callback";

          this.transaction(sId,sUrl,sInDs,sOutDs,sArg,sfunc);
        }

        this.fn_callback = function(sId,nErrorCode,sErrorMSG)
        {
          if(nErrorCode == 0)
          {
        	trace('완료')
          }
        }
        });
        
        // Regist UI Components Event
        this.on_initEvent = function()
        {
            this.Button00.addEventHandler("onclick",this.Button00_onclick,this);
        };
        this.loadIncludeScript("Form_Work.xfdl");
        this.loadPreloadList();
        
        // Remove Reference
        obj = null;
    };
}
)();
